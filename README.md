# Vigenere Bruteforce

This program is a bruteforcing algorithm for the vigenere cipher based on entropy.

## Dependecies
`argparse`  
`numpy`

## Usage
Syntax: `main.py [-h] cipherfile keylength`

Run the command and wait for the input prompt. When the prompt shows up, then type the line number of the last line and work your way up.  
After every input, a list of possible key and cleartext combinations will appear. One of them will probably be the key and cleartext.

The maximum recommended key length is 6 which will take about 4 hours. Keys of the length 3-5 are fast to crack.

Multithreading is currently not supported.

## Disclamer
This project may not be used in an illegal manner. I am not responsible for any damages made with or by this project.
