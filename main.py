#!/usr/bin/env python3
import argparse
import depend

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("cipherfile", help="File containing your encrypted message")
    argparser.add_argument("keylength", type=int, help="Length of the key used to encrypt your message")
    args = argparser.parse_args()

    try:
        with open(args.cipherfile, "r") as fd:
            cipherstring = fd.read().rstrip()
    except FileNotFoundError:
        depend.eprint("File Not Found!")
        exit(1)

    bruteforcedata = depend.bruteforce(depend.alphabet, cipherstring, max_keylength=args.keylength, min_keylength=args.keylength)
    while True:
        try:
            zeile=int(input("Zeile: "))-1
        except (EOFError, KeyboardInterrupt):
            break
        depend.cyclic_itter(bruteforcedata[zeile][0], bruteforcedata[zeile][2])
