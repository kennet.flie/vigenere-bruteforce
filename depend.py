#!/usr/bin/env python3
import sys
import numpy as np

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def gen_alphabet():
    x=[]
    for e in range(ord("A"),ord("Z")+1): x.append(chr(e))
    return x

alphabet = gen_alphabet()

def increment_ctable(alphabet, ctable, itter=0):
    if itter == len(ctable) - 1:
        ctable.append(0)
    elif ctable[itter] == len(alphabet) - 1:
        ctable[itter] = 0
        increment_ctable(alphabet, ctable, itter=itter+1)
    else:
        ctable[itter] += 1
    return ctable

def export_ctable(alphabet, ctable):
    out = ""
    for i in ctable:
        out += alphabet[i]
    return out

def calc_entropy(labels, base=None):
    e = np.exp(1)
    value,counts = np.unique(labels, return_counts=True)
    norm_counts = counts / counts.sum()
    base = e if base is None else base
    return -(norm_counts * np.log(norm_counts)/np.log(base)).sum()

def vigenere_decrypt(key, inp, alphabet):
    out = ""
    inp = inp.upper()
    keymv = 0
    for i in range(len(inp)):
        if inp[i] in alphabet:
            inpcharid = alphabet.index(inp[i])
            keycharid = alphabet.index(key[(i-keymv)%len(key)])
            out += alphabet[(inpcharid-keycharid)%len(alphabet)]
        else:
            keymv+=1
            out += inp[i]
    return(out)

def cyclic_move(cleartext,num):
    enctext = ""
    for char in cleartext:
        enctext += chr((ord(char)-65+num)%26 + 65)
    return enctext

def cyclic_itter(keytext, enctext):
    for i in range(26):
        print(cyclic_move(keytext, i), ":", cyclic_move(enctext, -1*i))


def bruteforce(alphabet, inp, max_keylength=None, min_keylength=1):
    ctable = [-1]
    for i in range(min_keylength-1): ctable.append(0)
    max_keylength = len(inp) if max_keylength is None else max_keylength
    cracked=False
    cache=None
    output=[]
    counter=0
    while not cracked:
        i = increment_ctable(alphabet, ctable)
        if len(i) > max_keylength: break
        key = export_ctable(alphabet, ctable)
        out = vigenere_decrypt(key, inp, alphabet)
        entropy = calc_entropy(list(out))
        if cache is None or entropy<cache:
            counter+=1
            cache=entropy
            print(counter, key+":", entropy, out)
            output.append([key, entropy, out])
    return output
